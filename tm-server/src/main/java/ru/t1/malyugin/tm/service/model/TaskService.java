package ru.t1.malyugin.tm.service.model;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.model.IProjectService;
import ru.t1.malyugin.tm.api.service.model.ITaskService;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.repository.model.TaskRepository;

import java.util.Collections;
import java.util.List;

@Service
public class TaskService extends AbstractWBSService<Task> implements ITaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final TaskRepository taskRepository;

    public TaskService(
            @NotNull final IProjectService projectService,
            @NotNull final TaskRepository taskRepository
    ) {
        this.projectService = projectService;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    protected TaskRepository getRepository() {
        return taskRepository;
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        if (!StringUtils.isBlank(description)) task.setDescription(description);
        update(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) return Collections.emptyList();
        @Nullable final User user = userService.findOneById(userId.trim());
        if (user == null) throw new UserNotFoundException();
        return getRepository().findAllByUserAndProjectId(user, projectId.trim());
    }

    @Override
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        @Nullable final Project project = projectService.findOneById(userId.trim(), projectId);
        if (project == null) throw new ProjectNotFoundException();
        getRepository().setProjectById(taskId.trim(), project);
    }

    @Override
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId.trim(), projectId.trim())) throw new ProjectNotFoundException();
        getRepository().setProjectById(taskId.trim(), null);
    }

}