package ru.t1.malyugin.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface IDatabasePropertyService {

    @NotNull
    String getDatabaseUsername();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseFormatSql();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseHbm2ddlAuto();

    @NotNull
    String getCacheUseSecondLevel();

    @NotNull
    String getCacheUseQuery();

    @NotNull
    String getCacheUseMinimalPuts();

    @NotNull
    String getCacheRegionPrefix();

    @NotNull
    String getCacheConfigFile();

    @NotNull
    String getCacheRegionFactoryClass();

    @NotNull
    String getLiquibaseConfigPath();

    @NotNull
    String getLiquibaseInitToken();

}