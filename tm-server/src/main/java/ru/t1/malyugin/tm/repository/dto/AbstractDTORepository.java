package ru.t1.malyugin.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.malyugin.tm.dto.model.AbstractDTOModel;

@NoRepositoryBean
public interface AbstractDTORepository<M extends AbstractDTOModel> extends JpaRepository<M, String> {

}