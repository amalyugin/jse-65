package ru.t1.malyugin.tm.listener.scheme;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.scheme.SchemeDropRequest;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class SchemeDropListener extends AbstractSchemeListener {

    @NotNull
    private static final String NAME = "scheme-drop";

    @NotNull
    private static final String DESCRIPTION = "Drop scheme";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @EventListener(condition = "@schemeDropListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[SCHEME DROP]");
        System.out.println("ENTER INIT TOKEN: ");
        @NotNull final String initToken = TerminalUtil.nextLine();
        @NotNull final SchemeDropRequest request = new SchemeDropRequest(initToken);
        schemeEndpoint.dropScheme(request);
    }

}