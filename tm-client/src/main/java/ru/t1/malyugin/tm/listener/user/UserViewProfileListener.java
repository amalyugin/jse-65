package ru.t1.malyugin.tm.listener.user;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.dto.request.user.UserGetProfileRequest;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.event.ConsoleEvent;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "user-show";

    @NotNull
    private static final String DESCRIPTION = "show current user profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[USER SHOW PROFILE]");

        @NotNull final UserGetProfileRequest request = new UserGetProfileRequest(getToken());
        @Nullable final UserDTO user = userEndpoint.getProfile(request).getUser();

        renderProfile(user);
    }

    private void renderProfile(@Nullable final UserDTO user) {
        if (user == null) return;
        @NotNull String result = "";
        @Nullable final String email = user.getEmail();
        @Nullable final String firstName = user.getFirstName();
        @Nullable final String lastName = user.getLastName();
        @Nullable final String middleName = user.getMiddleName();
        @NotNull final String id = user.getId();
        @NotNull final String login = user.getLogin();
        @NotNull final String role = user.getRole().getDisplayName();
        boolean isEmail = !StringUtils.isBlank(email);
        boolean isFIO = (!StringUtils.isBlank(firstName) || !StringUtils.isBlank(lastName) || !StringUtils.isBlank(middleName));
        boolean isFirstName = !StringUtils.isBlank(firstName);
        boolean isLastName = !StringUtils.isBlank(lastName);
        boolean isMiddleName = !StringUtils.isBlank(middleName);

        result += ("ID: " + id);
        result += ("\nLOGIN: " + login);
        result += ("\nROLE: " + role);
        result += (isEmail ? "\nEMAIL: " + email : "");
        result += (isFIO ? "\nFIO: "
                + (isFirstName ? firstName : "")
                + (isMiddleName ? " " + middleName : "")
                + (isLastName ? " " + lastName : "") : "");
        System.out.println(result);
    }

}