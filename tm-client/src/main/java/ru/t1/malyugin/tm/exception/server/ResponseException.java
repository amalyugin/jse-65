package ru.t1.malyugin.tm.exception.server;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.exception.AbstractException;

public final class ResponseException extends AbstractException {

    public ResponseException() {
        super("Some error occurred...");
    }

    public ResponseException(@NotNull final Throwable cause) {
        super(cause);
    }

    public ResponseException(@NotNull final String message) {
        super(message);
    }

}