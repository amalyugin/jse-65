package ru.t1.malyugin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class ProjectChangeStatusByIdListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-change-status-by-id";

    @NotNull
    private static final String DESCRIPTION = "Change project status by id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectChangeStatusByIdListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");

        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("ENTER STATUS: ");
        System.out.println(Status.renderValuesList());
        final int statusIndex = TerminalUtil.nextInteger();
        @NotNull final Status status = Status.getStatusByIndex(statusIndex);

        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken(), id, status);
        projectEndpoint.changeProjectStatusById(request);
    }

}