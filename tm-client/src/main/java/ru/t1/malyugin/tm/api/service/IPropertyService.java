package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationName();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getApplicationLog();

    @NotNull
    String getSoapLogin();

    @NotNull
    String getSoapPass();

    @NotNull
    String getAdminPass();

    @NotNull
    String getAdminLogin();

}