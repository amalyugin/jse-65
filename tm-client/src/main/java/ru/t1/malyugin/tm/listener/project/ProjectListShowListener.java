package ru.t1.malyugin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.dto.request.project.ProjectShowListRequest;
import ru.t1.malyugin.tm.enumerated.EntitySort;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class ProjectListShowListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-list";

    @NotNull
    private static final String DESCRIPTION = "Show project list";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectListShowListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT LIST]");

        System.out.print("ENTER SORT: ");
        System.out.print(EntitySort.renderValuesList());
        @Nullable final Integer sortIndex = TerminalUtil.nextIntegerSafe();
        @Nullable final EntitySort sort = EntitySort.getSortByIndex(sortIndex);

        @NotNull final ProjectShowListRequest request = new ProjectShowListRequest(getToken(), sort);
        @Nullable final List<ProjectDTO> projects = projectEndpoint.showProjectList(request).getProjectList();
        renderProjectList(projects);
    }

}