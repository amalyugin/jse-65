package ru.t1.malyugin.tm.exception.field;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.exception.AbstractException;

public abstract class AbstractFieldException extends AbstractException {

    protected AbstractFieldException() {
        super();
    }

    protected AbstractFieldException(@NotNull final String message) {
        super(message);
    }

    protected AbstractFieldException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    protected AbstractFieldException(@NotNull final Throwable cause) {
        super(cause);
    }

    protected AbstractFieldException(@NotNull final String message, @NotNull final Throwable cause,
                                     final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}