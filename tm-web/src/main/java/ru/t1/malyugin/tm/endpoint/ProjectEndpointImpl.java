package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.ProjectRestEndpoint;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.model.Project;

@RestController
@RequestMapping("/api/project")
public class ProjectEndpointImpl implements ProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping("/get/{id}")
    public Project get(@PathVariable("id") String id) {
        return projectService.findById(id);
    }

    @Override
    @PostMapping("/post")
    public void post(@RequestBody Project project) {
        projectService.add(project);
    }

    @Override
    @PutMapping("/put")
    public void put(@RequestBody Project project) {
        projectService.edit(project);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        projectService.deleteById(id);
    }

}