package ru.t1.malyugin.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.repository.TaskRepository;

import java.util.Random;

@Service
public class TaskService implements ITaskService {

    private final Random random = new Random();
    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Iterable<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public long count() {
        return taskRepository.count();
    }

    @Override
    @Transactional
    public void create() {
        int randomNumber = random.nextInt(101);
        Task task = new Task("T " + randomNumber, "DESC");
        taskRepository.save(task);
    }

    @Override
    public void add(Task task) {
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void deleteById(final String id) {
        if (!taskRepository.existsById(id)) return;
        taskRepository.deleteById(id);
    }

    @Override
    public void deleteAll(Iterable<Task> tasks) {
        taskRepository.deleteAll(tasks);
    }

    @Override
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public void edit(final Task task) {
        if (!taskRepository.existsById(task.getId())) return;
        taskRepository.save(task);
    }

    @Override
    public Task findById(final String id) {
        return taskRepository.findById(id).orElse(null);
    }

}