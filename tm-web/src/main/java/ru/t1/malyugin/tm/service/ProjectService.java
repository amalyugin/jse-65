package ru.t1.malyugin.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.repository.ProjectRepository;

import java.util.Random;

@Service
public class ProjectService implements IProjectService {

    private final Random random = new Random();
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Iterable<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public long count() {
        return projectRepository.count();
    }

    @Override
    @Transactional
    public void create() {
        int randomNumber = random.nextInt(101);
        Project project = new Project("P " + randomNumber, "DESC");
        projectRepository.save(project);
    }

    @Override
    public void add(final Project project) {
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void deleteById(final String id) {
        if (!projectRepository.existsById(id)) return;
        projectRepository.deleteById(id);
    }

    @Override
    public void deleteAll(Iterable<Project> projects) {
        projectRepository.deleteAll(projects);
    }

    @Override
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void edit(final Project project) {
        if (!projectRepository.existsById(project.getId())) return;
        projectRepository.save(project);
    }

    @Override
    public Project findById(final String id) {
        return projectRepository.findById(id).orElse(null);
    }

}