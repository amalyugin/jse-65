package ru.t1.malyugin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.malyugin.tm.model.Task;

public interface TaskRepository extends JpaRepository<Task, String> {

}