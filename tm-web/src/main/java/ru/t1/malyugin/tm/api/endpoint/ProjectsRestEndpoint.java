package ru.t1.malyugin.tm.api.endpoint;

import ru.t1.malyugin.tm.model.Project;

public interface ProjectsRestEndpoint {

    Iterable<Project> get();

    long count();

    void delete(Iterable<Project> projects);

    void clear();

}