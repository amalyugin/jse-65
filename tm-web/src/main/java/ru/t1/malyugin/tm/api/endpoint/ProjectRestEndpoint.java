package ru.t1.malyugin.tm.api.endpoint;

import ru.t1.malyugin.tm.model.Project;

public interface ProjectRestEndpoint {

    Project get(String id);

    void post(Project project);

    void put(Project project);

    void delete(String id);

}