package ru.t1.malyugin.tm.api.endpoint;

import ru.t1.malyugin.tm.model.Task;

public interface TasksRestEndpoint {

    Iterable<Task> get();

    int count();

    void delete(Iterable<Task> projects);

    void clear();

}