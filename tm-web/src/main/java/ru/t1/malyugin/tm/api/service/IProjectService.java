package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.model.Project;

public interface IProjectService {

    Iterable<Project> findAll();

    long count();

    void create();

    void add(Project project);

    void deleteById(String id);

    void deleteAll(Iterable<Project> projects);

    void clear();

    void edit(Project project);

    Project findById(String id);

}