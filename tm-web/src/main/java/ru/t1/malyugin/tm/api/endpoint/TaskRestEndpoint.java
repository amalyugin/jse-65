package ru.t1.malyugin.tm.api.endpoint;

import ru.t1.malyugin.tm.model.Task;

public interface TaskRestEndpoint {

    Task get(String id);

    void post(Task task);

    void put(Task task);

    void delete(String id);

}