package ru.t1.malyugin.tm.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.client.TaskRestEndpointClient;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.marker.IntegrationCategory;
import ru.t1.malyugin.tm.model.Task;

@Category(IntegrationCategory.class)
public class TaskTest {

    private static final TaskRestEndpointClient CLIENT = TaskRestEndpointClient.getInstance();
    private final Task task1 = new Task("T1", "D1");
    private final Task task2 = new Task("T2", "D2");

    @Before
    public void before() {
        CLIENT.post(task1);
        CLIENT.post(task2);
    }

    @After
    public void after() {
        CLIENT.delete(task1.getId());
        CLIENT.delete(task2.getId());
    }


    @Test
    public void getByIdTest() {
        Assert.assertNotNull(CLIENT.get(task1.getId()));
    }

    @Test
    public void addTest() {
        final Task task = new Task("NEW", "NEW");
        CLIENT.post(task);
        Assert.assertNotNull(CLIENT.get(task.getId()));
        CLIENT.delete(task.getId());
    }

    @Test
    public void deleteByIdTest() {
        CLIENT.delete(task1.getId());
        Assert.assertNull(CLIENT.get(task1.getId()));
        Assert.assertNotNull(CLIENT.get(task2.getId()));
    }

    @Test
    public void editTest() {
        Assert.assertNotNull(CLIENT.get(task1.getId()));
        task1.setStatus(Status.IN_PROGRESS);
        task1.setDescription("NEW");
        CLIENT.put(task1);
        final Task updatedProject = CLIENT.get(task1.getId());
        Assert.assertEquals(task1.getId(), updatedProject.getId());
        Assert.assertEquals(task1.getStatus(), updatedProject.getStatus());
        Assert.assertEquals(task1.getDescription(), updatedProject.getDescription());
    }

}